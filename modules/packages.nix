{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    wget
    git
    zip
    unzip
  ];
  fonts.fonts = with pkgs; [
    fira-code
    fira-code-symbols
  ];
}


