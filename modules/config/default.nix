{config , pkgs , ...}:
{
  services.xserver = {
    enable = false;
    #autorun = false;
    exportConfiguration = false;
    layout = "us";
    xkbOptions = "eurosign:e";
    windowManager = {
   #   bspwm = {
   #     enable = true;
   #     configFile = "/home/me/.config/bspwm/bspwmrc";
   #     sxhkd.configFile = "/home/me/.config/sxhkd/sxhkdrc";
   #   };
    };
   # libinput = {
   #   enable = true;
   #   tapping = false;
   # };
   # displayManager = {
   #   startx.enable = false;
   #   lightdm.enable = true;
   # };
  };
}
