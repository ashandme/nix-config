{ config, pkgs, ... }:
{
  boot = {
    kernelParams = ["acpi_backlight=native"];
    loader = {
      efi = {
        efiSysMountPoint = "/boot";
        canTouchEfiVariables = true;
      };
      grub = {
        enable = true;
        devices = ["nodev"];
        efiSupport = true;
        useOSProber = true;
      };
    };
    kernelPackages = pkgs.linuxPackages_latest;
    cleanTmpDir = true;
  };
  fileSystems."/home/me" = {
    device = "/dev/disk/by-label/home";
    fsType = "ext4";
  };
}
