My NixOs Configuration
======================

keep in mind that this configuration is specially made for my hardware and my personal use

How i use NixOS?
----------------

I use it like a normal person, it's like using Archlinux. It is necessary to know that there are two package managers, one in the root that this config is specifically for that manager and another in the user called "nix" that of the root I use it only to place the essential things that I want on my system since at putting things in the root also installs other users, such as pacman and apt, so when installing programs that are not important to the system it is better to use the manager that comes with the user.
