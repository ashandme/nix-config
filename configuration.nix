{ config, pkgs, lib, ... }:
#let unstableTarball = 
  #fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz;
#in
{
  imports =
    [
      ./hardware-configuration.nix
      ./modules
    ];
  
  nixpkgs.config = {
    packageOverrides = pkgs: {
      #unstable = import unstableTarball {
        ##config = config.nixpkgs.config;
      #};
    };
    allowUnfree = true;
  };
  powerManagement = {
    enable = true;
    powertop.enable = true;
  };
  networking = {
    hostName = "ash-notebook";
    wireless = {
      enable = true;
      networks = {
        TCH-1694467.psk = "24RVqCd8NqnhEMJ66W";
      };
    };
    useDHCP= false;
    interfaces.wlp1s0.useDHCP = true;
  };
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    keyMap = "us";
  };
  time.timeZone = "America/Argentina/Buenos_Aires";
  programs = {
    zsh = {
      enable = true;
      ohMyZsh = {
        enable = true;
        theme = "agnoster";
      };
      shellAliases = {
        vim = "nvim";
      };
    };
    mtr.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    sway = {
      enable = true;
      extraPackages = with pkgs; [ swaylock swayidle xwayland termite firefox-wayland];
    };
  };

  sound.enable = true;
  hardware = {
    pulseaudio.enable = true;
    brillo.enable = true;
    cpu.amd.updateMicrocode = true;
    enableAllFirmware = true;
  };
  security = {
    sudo.enable = false;
    doas.enable = true;
  };

  services = {
    openssh.enable = true;
  };
  users.users.me = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "audio" ];
    shell = pkgs.zsh;
    useDefaultShell = true;
  };
  system = {
    stateVersion = "20.09";
    autoUpgrade = {
      enable = true;
      dates = "weekly";
    };
  };
}
